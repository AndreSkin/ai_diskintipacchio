from keras.models import load_model
import numpy as np
import keras.backend as K
from PIL import Image
from matplotlib import pyplot as plt
from dotenv import load_dotenv
import os
from os.path import join, dirname

import keras.losses

def my_loss(loss_info, y_pred):
    y_pred = y_pred[:, :, :, 0]
    y_true = loss_info[:, :, :, 0]
    mask = loss_info[:, :, :, 1]
    loss = K.mean(((y_true - y_pred) * mask) ** 2, axis=(1, 2))
    loss = K.sum(loss)

    return loss

# Load the dataset path from a .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
test_img_path = os.environ.get("testImgPath")

keras.losses.custom_loss = my_loss

# Load the image from the test image path defined in the .env file
img = np.array(Image.open(test_img_path))
img.resize((192, 256, 3))

images_list = []
images_list.append(np.array(img))
x = np.asarray(images_list)

script_dir = os.path.dirname(os.path.abspath(__file__))


# Iterate over all subdirectories in the current directory
for subdir, dirs, files in os.walk(script_dir):
    for file in files:
        # Check if the file is an .h5 file
        if file.endswith('.h5'):
            # Load the model from the .h5 file
            model_path = os.path.join(subdir, file)
            try:
                model = load_model(model_path, custom_objects={'my_loss': my_loss})
                print(f"Loaded model from {model_path}")
            except ValueError as e:
                print(f"Error loading model from {model_path}: {e}")
                continue

            a = model.predict(x).round()

            # a.reshape((-1,192,256,2))
            elem = a[0]
            elem = elem[:, :, 0]

            min_val = elem.min()
            max_val = elem.max()

            # Trasforma la matrice in una matrice di colori RGB
            rgb_matrix = np.zeros((elem.shape[0], elem.shape[1], 3), dtype=np.uint8)
            for i in range(elem.shape[0]):
                for j in range(elem.shape[1]):
                    # Mappa il valore della matrice al corrispondente valore RGB
                    ratio = (elem[i,j] - min_val) / (max_val - min_val)
                    rgb_matrix[i,j,0] = int(ratio * 255)
                    rgb_matrix[i,j,1] = int((1 - ratio) * 255)
                    rgb_matrix[i,j,2] = int(ratio * 255)

            # Salva l'immagine nella stessa cartella del .h5 come nomecartella-img-ds.png dove nomecartella è il nome della cartella in cui si trova
            img_filename = subdir.split(os.sep)[-1] + '-img-test.png'
            img_filepath = os.path.join(subdir, img_filename)
            img = Image.fromarray(rgb_matrix)
            img.save(img_filepath)
