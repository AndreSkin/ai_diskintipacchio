from keras.models import load_model
import numpy as np
import keras.backend as K
from PIL import Image
from matplotlib import pyplot as plt
from dotenv import load_dotenv
import os
from os.path import join, dirname

import keras.losses

def my_loss(loss_info, y_pred):
    y_pred = y_pred[:, :, :, 0]
    y_true = loss_info[:, :, :, 0]
    mask = loss_info[:, :, :, 1]
    loss = K.mean(((y_true - y_pred) * mask) ** 2, axis=(1, 2))
    loss = K.sum(loss)

    return loss

# Load the dataset path from a .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
test_img_path = os.environ.get("testImgPath")

# result_path = os.environ.get("resultPath")

keras.losses.custom_loss = my_loss

# Load the image from the test image path defined in the .env file
img = np.array(Image.open(test_img_path))
img.resize((192, 256, 3))

images_list = []
images_list.append(np.array(img))
x = np.asarray(images_list)

script_dir = os.path.dirname(os.path.abspath(__file__))


# Iterate over all subdirectories in the current directory
for subdir, dirs, files in os.walk(script_dir):
    for file in files:
        # Check if the file is an .h5 file
        if file.endswith('.h5'):
            # Load the model from the .h5 file
            model_path = os.path.join(subdir, file)
            try:
                model = load_model(model_path, custom_objects={'my_loss': my_loss})
                print(f"Loaded model from {model_path}")
            except ValueError as e:
                print(f"Error loading model from {model_path}: {e}")
                continue

            a = model.predict(x).round()

            # a.reshape((-1,192,256,2))
            elem = a[0]
            elem = elem[:, :, 0]

            min_val = elem.min()
            max_val = elem.max()
            # Salva l'immagine nella stessa cartella del .h5 come nomecartella_depth_map_computed.npy dove nomecartella è il nome della cartella in cui si trova
            npy_filename = subdir.split(os.sep)[-1] + '_depth_map_computed_3.npy'
            npy_filepath = os.path.join(subdir, npy_filename)
            np.save(npy_filepath, elem)
