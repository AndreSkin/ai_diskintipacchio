import numpy as np
import sys
import os
from os.path import join, dirname
from tensorflow import keras
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from dotenv import load_dotenv
from datetime import datetime
from uuid import uuid4
import keras.backend as K
import requests

class DataGenerator(keras.utils.Sequence):
    def __init__(self, dir_path, element_names, batch_size=32, dim=(192, 256), n_channels=3, shuffle=True):
        # Inizializza le variabili di istanza della classe
        self.dir_path = dir_path
        self.element_names = element_names
        self.batch_size = batch_size
        self.dim = dim
        self.n_channels = n_channels
        self.shuffle = shuffle
        # Chiama il metodo on_epoch_end per inizializzare gli indici del batch
        self.on_epoch_end()

    def __len__(self):
        # Calcola il numero di batch per epoca
        return int(np.floor(len(self.element_names) / self.batch_size))

    def __getitem__(self, index):
        # Genera gli indici del batch corrente
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Trova i nomi degli elementi del batch corrente
        element_names_temp = [self.element_names[k] for k in indexes]
        # Genera i dati del batch corrente
        X, y, mask = self.__data_generation(element_names_temp)
        # Combina y e mask in un unico tensore per passarlo al modello come target
        # loss_info = tf.stack([y[:, :, :, 0], mask], axis=-1)
        loss_info = tf.stack([y, mask], axis=-1)
        return X, loss_info

    def on_epoch_end(self):
        # Aggiorna gli indici dopo ogni epoca
        self.indexes = np.arange(len(self.element_names))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, element_names_temp):
        # Inizializza i tensori per contenere i dati del batch corrente
        # X = np.empty((self.batch_size, *self.dim, self.n_channels))
        # y = np.empty((self.batch_size, *self.dim))
        # mask = np.empty((self.batch_size, *self.dim))        
        
        X = np.empty((128,192,256,3))
        y = np.empty((128,192,256))
        mask = np.empty((128,192,256))
        
        # Carica i dati del batch corrente dal disco
        for i, ID in enumerate(element_names_temp):
            X[i] = plt.imread(os.path.join(self.dir_path, ID + '.png'))
            y[i] = np.load(os.path.join(self.dir_path, ID + '_depth.npy'))
            mask[i] = np.load(os.path.join(self.dir_path,ID + '_depth_mask.npy'))
            
        return X, y, mask


def my_loss(loss_info, y_pred):
    y_pred = y_pred[:, :, :, 0]
    # print(loss_info.shape)
    y_true = loss_info[:, :, :, 0]
    mask = loss_info[:, :, :, 1]

    # print(type(y_true).__name__)
    # print(type(y_pred).__name__)
    # print(y_true.shape)
    # print(y_pred.shape)


    loss = K.mean(((y_true - y_pred) * mask) ** 2, axis=(1, 2))
    # print(loss.shape)
    loss = K.sum(loss)
    # print(loss.shape)

    return loss

# Load the dataset path from a .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
dir_path = os.environ.get("Dataset")

fixed_length = 27
elem_num = 25000

element_names = []
file_list = np.array(os.listdir(dir_path))
file_list.sort()
for file in file_list:
    element_name = file[:fixed_length]
    if element_name not in element_names:
        element_names.append(element_name)
    if len(element_names) == elem_num:
        break

requests.post("https://ntfy.sh/ai_diskintipacchio", data="Finito element_names".encode(encoding='utf-8'))

# Split the data into training and validation sets
train_size = int(0.8 * len(element_names))
val_test_size = len(element_names) - train_size
val_size = int(0.5 * val_test_size)
test_size = val_test_size - val_size

train_element_names = element_names[:train_size]
val_element_names = element_names[train_size:train_size+val_size]
test_element_names = element_names[train_size+val_size:]

# Create a data generator for the training and validation data
batch_size = 128
train_generator = DataGenerator(dir_path=dir_path,element_names=train_element_names,batch_size=batch_size)
val_generator = DataGenerator(dir_path=dir_path,element_names=val_element_names,batch_size=batch_size)
test_generator = DataGenerator(dir_path=dir_path,element_names=test_element_names,batch_size=batch_size)

print("Elements: ", len(file_list))
print("Number of elements: ", len(element_names))
print("Training set size: ", len(train_element_names))
print("Validation set size: ", len(val_element_names))
print("Test set size: ", len(test_element_names))

# early stopping callback
callback = keras.callbacks.EarlyStopping(monitor='loss', patience=5, mode='min')

# Define the model
base_model = keras.Sequential([
    keras.layers.Conv2D(filters=16, kernel_size=2, padding="same", activation="relu", input_shape=(192,256,3)),
    keras.layers.Conv2D(filters=32, kernel_size=2, padding="same", activation ="relu"),
    keras.layers.MaxPooling2D(pool_size=2),
    keras.layers.Conv2D(filters=64, kernel_size=2, padding="same", activation="relu"),
    keras.layers.MaxPooling2D(pool_size=2),
    keras.layers.Conv2D(filters=64, kernel_size=2, padding="same", activation="relu"),
    keras.layers.UpSampling2D(size=2),
    keras.layers.Conv2D(filters=64, kernel_size=2, padding="same", activation="relu"),
    keras.layers.UpSampling2D(size=2),
    keras.layers.Conv2D(filters=1, kernel_size=2, padding="same", activation="relu")
])

inputs = keras.Input(shape=(192, 256, 3))

#output 1 and 2 have the same shape
output1 = base_model(inputs)[:, :, :, 0]
output2 = tf.zeros_like(inputs)[:, :, :, 0]
#We stack out1 and out2 in order to pass mask and train values to model afterwards
main_output = tf.stack([output1, output2], axis=-1)

model = keras.Model(inputs=inputs, outputs=main_output) 

# Compile the model with the mean squared error loss function and the Adam optimizer
model.compile(optimizer='adam', loss=my_loss, metrics=['accuracy','mse'])

# Train the model on the training data and validate it on the validation data
history = model.fit(train_generator,validation_data=val_generator,epochs=100,callbacks=[callback],verbose=1)

requests.post("https://ntfy.sh/ai_diskintipacchio", data="Finita fit".encode(encoding='utf-8'))

eventid = datetime.now().strftime('%Y-%m-%d_%H_%M_%S') + str(uuid4())
os.mkdir('./models/'+eventid)
for element in history.history.keys():
    print("History",element,":",history.history[element])
    plt.figure()
    plt.plot(history.history[element])
    plt.title(element)
    plt.ylabel(element)
    plt.xlabel('Epoch')
    plt.savefig(f'./models/{eventid}/{element}.png')
    plt.close()

# Evaluate the trained model on the test data and print the mean absolute error
results = model.evaluate(test_generator,verbose=0)
for elem in model.metrics_names:
    print("Evaluation",elem,":",results[model.metrics_names.index(elem)])

# Saving trained model
model.save('./models/'+eventid+'/model.h5')
requests.post("https://ntfy.sh/ai_diskintipacchio", data="Fine, evviva".encode(encoding='utf-8'))
