import numpy as np
import sys
import os
from os.path import join, dirname
from tensorflow import keras
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from dotenv import load_dotenv
from datetime import datetime
from uuid import uuid4
import keras.backend as K

def my_loss(loss_info, y_pred):
    y_pred = y_pred[:, :, :, 0]
    # print(loss_info.shape)
    y_true = loss_info[:, :, :, 0]
    mask = loss_info[:, :, :, 1]

    # print(type(y_true).__name__)
    # print(type(y_pred).__name__)
    # print(y_true.shape)
    # print(y_pred.shape)


    loss = K.mean(((y_true - y_pred) * mask) ** 2, axis=(1, 2))
    # print(loss.shape)
    loss = K.sum(loss)
    # print(loss.shape)

    return loss


# Load the dataset path from a .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
dir_path = os.environ.get("SampleDataset")

# Load images and depth maps from the directory, and remove invalid points using depth mask
fixed_length = 27
element_names = []
images = []
depth_maps = []
masks = []
min = 0
minimumArray = []
mediumArray = []

for file in os.listdir(dir_path):
    element_name = file[:fixed_length]
    if element_name not in element_names:
        element_names.append(element_name)
        image = plt.imread(os.path.join(dir_path, element_name + '.png'))
        depth_map = np.load(os.path.join(dir_path, element_name + '_depth.npy'))
        depth_mask = np.load(os.path.join(dir_path, element_name + '_depth_mask.npy'))

        # Remove invalid points
        # min = np.min(depth_mask)
        # medium = (min + 1)/2

        # minimumArray.append(min)
        # mediumArray.append(medium)

        # for i in range(depth_mask.shape[0]):
        #     for j in range(depth_mask.shape[1]):
        #         if depth_mask[i][j] < medium:
        #             depth_mask[i][j] = 0
        #         else:
        #             depth_mask[i][j] = 1

        images.append(image)
        depth_maps.append(depth_map)
        masks.append(depth_mask)


# Prepare the data for training by converting the images and depth maps into numpy arrays, and expanding dimensions
images = np.array(images)
depth_maps = np.array(depth_maps)
depth_maps = np.expand_dims(depth_maps, axis=-1)
masks= np.array(masks)

# Split the data into training and validation sets
img_splitted, img_val, maps_splitted, maps_val,mask_splitted,mask_val = train_test_split(images, depth_maps,masks, train_size=0.8)
img_train, img_test, maps_train, maps_test,mask_train,mask_test = train_test_split(img_splitted, maps_splitted,mask_splitted, train_size=0.9)

# early stopping callback
callback = keras.callbacks.EarlyStopping(monitor='loss', patience=5, mode='min')


# Define the model
base_model = keras.Sequential([
    keras.layers.Conv2D(filters=16, kernel_size=2, padding="same", activation="relu", input_shape=(192,256,3)),
    keras.layers.Conv2D(filters=32, kernel_size=2, padding="same", activation ="relu"),
    keras.layers.MaxPooling2D(pool_size=2),
    keras.layers.Conv2D(filters=64, kernel_size=2, padding="same", activation="relu"),
    keras.layers.MaxPooling2D(pool_size=2),
    keras.layers.Conv2D(filters=64, kernel_size=2, padding="same", activation="relu"),
    keras.layers.UpSampling2D(size=2),
    keras.layers.Conv2D(filters=64, kernel_size=2, padding="same", activation="relu"),
    keras.layers.UpSampling2D(size=2),
    keras.layers.Conv2D(filters=1, kernel_size=2, padding="same", activation="relu")
])

inputs = keras.Input(shape=(192, 256, 3))

#output 1 and 2 have the same shape
output1 = base_model(inputs)[:, :, :, 0]
output2 = tf.zeros_like(inputs)[:, :, :, 0]
#We stack out1 and out2 in order to pass mask and train values to model afterwards
main_output = tf.stack([output1, output2], axis=-1)

model = keras.Model(inputs=inputs, outputs=main_output) 

# Stack the mask and depth map arrays into a single tensor with same shape as the output
loss_info_train = tf.stack([maps_train[:, :, :, 0], mask_train], axis=-1)
loss_info_val = tf.stack([maps_val[:, :, :, 0], mask_val], axis=-1)
loss_info_test = tf.stack([maps_test[:, :, :, 0], mask_test], axis=-1)

# Compile the model with the mean squared error loss function and the Adam optimizer
model.compile(optimizer='adam', loss=my_loss, metrics=['mse'])
#print(model.summary())
# Train the model on the training data and validate it on the validation data
history = model.fit(img_train, loss_info_train, validation_data=(img_val, loss_info_val), callbacks=[callback], verbose=1, epochs=5)
# Evaluate the trained model on the test data and print the mean absolute error
results = model.evaluate(img_test, loss_info_test)

for elem in model.metrics_names:
    print("Evaluation",elem,":",results[model.metrics_names.index(elem)])

# print('Test accuracy:', test_accuracy, '\nTest loss:', test_loss)
# Make predictions on the test data using the trained model
predictions = model.predict(images)
print(tf.reduce_sum(predictions[:, :, :, 0]))
print(tf.reduce_sum(predictions[:, :, :, 1]))


# Plot the training and validation loss
# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('Model loss')
# plt.ylabel('Loss')
# plt.xlabel('Epoch')
# plt.legend(['Loss', 'Val_loss'], loc='upper right')
# plt.show()

# Saving trained model
# eventid = datetime.now().strftime('%Y-%m-%d_%H_%M_%S') + str(uuid4())

# os.makedirs('./models/'+eventid)
# model.save('./models/'+eventid+'/model.h5')

# with open('./models/'+eventid+'/data.txt', 'w') as f:
#     f.write('Medium:'+str(mediumArray)+'\n Minimum: '+str(minimumArray))

# with open('./train.py','r') as fp, open('./models/'+eventid+'/code.txt', "w") as tp:
#     lines = fp.readlines()
#     found = False
#     for line in lines:
#         if found:
#             tp.writelines(line)
#             if ("])" in line):
#                 break

#         if (("base_model = keras.Sequential([" in line) and (not found)):
#             tp.writelines(line)
#             found = True
