tensorflow==2.8
python-dotenv
scikit-learn
matplotlib
datetime
uuid
Pillow
protobuf==3.20.*
