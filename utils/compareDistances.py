import os
import numpy as np
import json
import re

path = '/home/alessio/Desktop/dataset/00006_00081_indoors_350_010_depth.npy'
depth_map_original = np.load(path)


# Utilizza una regex per trovare il testo tra "indoor" e "depth"
pattern = r'indoors(.*?)_depth'
re_search= re.search(pattern, path, re.DOTALL)

# Verifica se è stata trovata una corrispondenza
if re_search:
    result_key = re_search.group(1)
    print(result_key)
else:
    print("Nessuna corrispondenza trovata.")

dir_path = '/home/alessio/Desktop/conTutto'
for path in os.listdir(dir_path):
   # print(path)
    for file in os.listdir(os.path.join(dir_path,path)):
        if file.endswith('2.npy'):
            #print('Shape original: ',depth_map_original.shape)
            depth_map_computated = np.load(os.path.join(dir_path,path,file))

            # for i in range(depth_map_computated.shape[0]):
            #     for j in range(depth_map_computated.shape[1]):
            #         depth_map_computated[i][j] = depth_map_computated[i][j] * np.max(depth_map_original) / depth_map_original[i][j]

            depth_map_computated = (depth_map_computated / depth_map_computated.max()) * depth_map_original.max()

            print(depth_map_original.max())

            #print('Shape computated: ',depth_map_computated.shape)
            # Calcola la differenza tra le matrici
            differenza = np.abs(depth_map_original - depth_map_computated)

            # Calcola la somma totale dei valori originali nelle due matrici
            somma_originale = np.sum(depth_map_original) + np.sum(depth_map_computated)

            # Calcola la somma totale delle differenze
            somma_differenze = np.sum(differenza)

            # Calcola la differenza percentuale
            differenza_percentuale = (somma_differenze / somma_originale) * 100

            print(f"Differenza percentuale: {differenza_percentuale}% con modello ",path)

            toSave = {
                #"differenza": differenza,
                "diff_percentuale": differenza_percentuale,
                "somma_originale": somma_originale,
                "somma_differenze": somma_differenze,
            }

                        # Specifica il percorso del file in cui desideri salvare i dati JSON
           # percorso_del_file = os.path.join(dir_path,path, "distance3_"+result_key+".json")

            #print(toSave)

            #result = json.dumps(toSave)  # Formatta i dati JSON con indentazione di 4 spazi    
            # Salva i dati nel file JSON
            #with open(percorso_del_file, "w") as file_json:
            #    json.dump(result, file_json)
       