import numpy as np
from PIL import Image
import os
from os.path import join, dirname
from tqdm import tqdm

from dotenv import load_dotenv

# Load the dataset path from a .env file
dotenv_path = join('../.env')
load_dotenv(dotenv_path)
dir_path = os.environ.get("TestDataset")
print(dir_path)
# carica l'immagine
# Definisci il percorso dell'immagine originale e della cartella di destinazione
destination_img_folder = "./shrink/"


# Crea la cartella se non esiste giÃ 
if not os.path.exists(destination_img_folder):
    os.makedirs(destination_img_folder)

pbar = tqdm(total=len([entry for entry in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path, entry))]))
    #for scene in os.listdir(dir_path):
        # for scan in os.listdir(dir_path+scene):
        #     for file in os.listdir(dir_path+scene+'/'+scan):
    #            destination_img_path = os.path.join(destination_img_folder, file)
    #           if (file.endswith(".png")):
    #              # Apri l'immagine
                #     img = Image.open(dir_path+scene+'/'+scan+'/'+file)
            #     # riduci le dimensioni dell'immagine a 256x192 pixel
            #     img = img.resize((256, 192))
            #     img.save(destination_img_path)
        
            # if (file.endswith("depth_mask.npy")):
            #     mask = np.load(dir_path+scene+'/'+scan+'/'+file)
            #     #copia mask
            #     RM=mask


            # if (file.endswith("depth.npy")):
            #     depth = np.load(dir_path+scene+'/'+scan+'/'+file)
            #     depth_copy = depth
                #se un pixel è nullo, sostituisci il valore con la media dei valori dei pixel vicini
fixed_length = 27
element_names = []
for file in os.listdir(dir_path):
    element_name = file[:fixed_length]
    if element_name not in element_names:
        element_names.append(element_name)
        #image = plt.imread(os.path.join(dir_path, element_name + '.png'))
        #depth_map = np.load(os.path.join(dir_path, element_name + '_depth.npy'))
        # depth_mask = np.load(os.path.join(dir_path, element_name + '_depth_mask.npy'))
        # depth_map[depth_mask < 0.5] = 0  # remove invalid points
       # images.append(image)
        #depth_maps.append(depth_map)
# for scan in os.listdir(dir_path+scene):
#     for file in os.listdir(dir_path+scene+'/'+scan):
        destination_img_path = os.path.join(destination_img_folder, element_name)

        # Apri l'immagine
        img = Image.open(dir_path+element_name+'.png')
        # riduci le dimensioni dell'immagine a 256x192 pixel
        img = img.resize((256, 192))
        img.save(destination_img_path+'.png')

        mask = np.load(dir_path+element_name+'_depth_mask.npy')
        #copia mask
        RM=mask


        depth = np.load(dir_path+element_name+'_depth.npy')
        depth_copy = depth
        for i in range(depth_copy.shape[0]):
            for j in range(depth_copy.shape[1]):
                if RM[i,j] == 0:
                    print("pixel nullo", i, j)
                    depth_copy[i,j] = np.mean(depth_copy[i - 1, j -1] + depth_copy[i - 1, j] + depth_copy[i - 1, j + 1] + depth_copy[i, j - 1] + depth_copy[i, j + 1] + depth_copy[i + 1, j - 1] + depth_copy[i + 1, j] + depth_copy[i + 1, j + 1]) 
        # crea le matrici RM
        RD = np.zeros((192, 256))
        # calcola la media dei valori di Depth e Mask per i pixel eliminati corrispondenti
        for i in range(192):
            for j in range(256):
                RD[i,j] = np.mean(depth_copy[i*4:(i+1)*4, j*4:(j+1)*4])
        # salva la matrice RD
        np.save(destination_img_path+'_depth.npy', RD)
    pbar.update(1)