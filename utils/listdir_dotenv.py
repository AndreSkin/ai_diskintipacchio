#https://stackoverflow.com/questions/41546883/what-is-the-use-of-python-dotenv
import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

path = os.environ.get("DS_PATH")
print(path)

print(os.listdir(path))