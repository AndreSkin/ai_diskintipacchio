import numpy as np
import os
from os.path import join, dirname
from matplotlib import pyplot as plt
import os
import shutil
from dotenv import load_dotenv

# Load the dataset path from a .env file
dotenv_path = join('../.env')
load_dotenv(dotenv_path)
dir_path = os.environ.get("SampleDataset")
destination_path = os.environ.get("DestinationFolder")

# Load images and depth maps from the directory, and remove invalid points using depth mask
fixed_length = 27
element_names = []
images = []
depth_maps = []

for file in os.listdir(dir_path):
    element_name = file[:fixed_length]
    if element_name not in element_names:
        element_names.append(element_name)
        shutil.copyfile(os.path.join(dir_path, element_name + '.png'), os.path.join(destination_path, element_name + '.png'))
        depth_map = np.load(os.path.join(dir_path, element_name + '_depth.npy'))
        depth_mask = np.load(os.path.join(dir_path, element_name + '_depth_mask.npy'))
        depth_map[depth_mask < 0.5] = 0  # remove invalid points
        with open(os.path.join(destination_path, element_name + '_depth.npy'), 'wb') as f:
            np.save(f, depth_map)
