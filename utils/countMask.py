import numpy as np
import os
from os.path import join, dirname
from tqdm import tqdm

elements = []

from dotenv import load_dotenv
dotenv_path = join('../.env')
load_dotenv(dotenv_path)
dir_path = os.environ.get("Dataset")
print(dir_path)


for scene in os.listdir(dir_path):
        for scan in os.listdir(dir_path+scene):
             for file in os.listdir(dir_path+scene+'/'+scan):
                if file.endswith('_depth_mask.npy'):
                        mask = np.load(dir_path+scene+'/'+scan+'/'+file)
                        for i in range(mask.shape[0]):
                            for j in range(mask.shape[1]):
                                    if mask[i,j] == 0:
                                        if file not in elements:
                                            elements.append(file)
                                            print("Found:",file)
                                            break

print(elements)       
print("Totale file:",len(elements))                       
with open('corrupted.txt','w') as file:
      file.write(str(elements))