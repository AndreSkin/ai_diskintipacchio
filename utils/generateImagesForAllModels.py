import os
import numpy as np
import json
import re
from PIL import Image

dir_path = '/home/alessio/Desktop/conTutto/Slurm'
for path in os.listdir(dir_path):
   # print(path)
    for file in os.listdir(os.path.join(dir_path,path)):
        if file.endswith('3.npy'):
            #print('Shape original: ',depth_map_original.shape)
            elem = np.load(os.path.join(dir_path,path,file))
            #print('Shape computated: ',depth_map_computated.shape)
            # Calcola la differenza tra le matrici
           
            min_val = elem.min()
            max_val = elem.max()

            print(min_val)
            print(max_val)

            # Trasforma la matrice in una matrice di colori RGB
            rgb_matrix = np.zeros((elem.shape[0], elem.shape[1], 3), dtype=np.uint8)
            for i in range(elem.shape[0]):
                for j in range(elem.shape[1]):
                    # Mappa il valore della matrice al corrispondente valore RGB
                    ratio = (elem[i,j] - min_val) / (max_val - min_val)
                    rgb_matrix[i,j,0] = int(ratio * 255)
                    rgb_matrix[i,j,1] = int((1 - ratio) * 255)
                    rgb_matrix[i,j,2] = int(ratio * 255)

            # Mostra la matrice RGB come immagine
            img = Image.fromarray(rgb_matrix)
            img.save(os.path.join('/home/alessio/Desktop/images/350_020',path+'.png'))