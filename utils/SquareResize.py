import os
from os.path import join, dirname
from dotenv import load_dotenv
from PIL import Image
import numpy as np

# Load the dataset path from a .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
dir_path = os.environ.get("Dataset")
newDataset = os.environ.get("NewDataset")

target_size = (100, 100)

for filename in os.listdir(dir_path):
    file_path = os.path.join(dir_path, filename)
    if filename.endswith('.png'):
        # Process image file
        image = Image.open(file_path)
        width, height = image.size
        from_left = (width - target_size[0]) // 2
        from_right = width - from_left
        from_top = (height - target_size[1]) // 2
        from_bottom = height - from_top
        cropped_image = image.crop((from_left, from_top, from_right, from_bottom))
        cropped_image.save(os.path.join(newDataset, filename))
    elif filename.endswith('.npy'):
        # Process numpy array file
        matrix = np.load(file_path)
        height, width = matrix.shape
        from_left = (width - target_size[0]) // 2
        from_right = width - from_left
        from_top = (height - target_size[1]) // 2
        from_bottom = height - from_top
        cropped_matrix = matrix[from_top:from_bottom, from_left:from_right]
        np.save(os.path.join(newDataset, filename), cropped_matrix)

    # Set file permissions to 777
    os.chmod(file_path, 0o777)