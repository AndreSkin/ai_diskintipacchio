from dotenv import load_dotenv
import os
from os.path import join, dirname
import numpy as np
from PIL import Image, ImageEnhance
import requests

# Load the dataset path from a .env file
dotenv_path = join("../", '.env')
load_dotenv(dotenv_path)
dir_path = os.environ.get("AugmentedDataset")
directory = dir_path

def modify_brightness(image, value):
    image = Image.open(image)
    image = image.point(lambda x: x * (1 + value / 100))
    return image

def modify_saturation(image, value):
    image = Image.open(image)
    enhancer = ImageEnhance.Color(image)
    image = enhancer.enhance(1 + value / 100)
    return image

for filename in os.listdir(directory):
    if filename.endswith('.png'):
        # Carica l'immagine originale e crea una copia specchiata orizzontalmente
        image = Image.open(os.path.join(directory, filename))
        flipped_image = image.transpose(Image.FLIP_LEFT_RIGHT)
        flipped_image.save(os.path.join(directory, 'h' + filename[1:]))

        # Carica le matrici originali e crea copie specchiate orizzontalmente
        depth = np.load(os.path.join(directory, filename.replace('.png', '_depth.npy')))
        depth_mask = np.load(os.path.join(directory, filename.replace('.png', '_depth_mask.npy')))
        np.save(os.path.join(directory, 'h' + filename.replace('.png', '_depth.npy')[1:]), np.fliplr(depth))
        np.save(os.path.join(directory, 'h' + filename.replace('.png', '_depth_mask.npy')[1:]), np.fliplr(depth_mask))

requests.post("https://ntfy.sh/ai_diskintipacchio", data="1/6😀".encode(encoding='utf-8'))

for filename in os.listdir(directory):
    if filename.endswith('.png'):
        # Carica l'immagine originale e crea una copia capovolta sottosopra
        image = Image.open(os.path.join(directory, filename))
        flipped_image = image.transpose(Image.FLIP_TOP_BOTTOM)
        new_filename = list(filename)
        new_filename[1] = 'v'
        new_filename = ''.join(new_filename)
        flipped_image.save(os.path.join(directory, new_filename))

        # Carica le matrici originali e crea copie capovolte sottosopra
        depth = np.load(os.path.join(directory, filename.replace('.png', '_depth.npy')))
        depth_mask = np.load(os.path.join(directory, filename.replace('.png', '_depth_mask.npy')))
        np.save(os.path.join(directory, new_filename.replace('.png', '_depth.npy')), np.flipud(depth))
        np.save(os.path.join(directory, new_filename.replace('.png', '_depth_mask.npy')), np.flipud(depth_mask))

requests.post("https://ntfy.sh/ai_diskintipacchio", data="2/6".encode(encoding='utf-8'))

for filename in os.listdir(directory):
    if filename.endswith('.png'):
        image_path = os.path.join(directory, filename)
        depth_path = os.path.join(directory, filename.replace('.png', '_depth.npy'))
        depth_mask_path = os.path.join(directory, filename.replace('.png', '_depth_mask.npy'))
        
        new_image = modify_brightness(image_path, 25)
        new_image_name = list(filename)
        new_image_name[2] = 'b'
        new_image_name = ''.join(new_image_name)
        new_image_path = os.path.join(directory, new_image_name)
        new_image.save(new_image_path)
        
        depth = np.load(depth_path)
        depth_mask = np.load(depth_mask_path)
        
        new_depth_path = os.path.join(directory, new_image_name.replace('.png', '_depth.npy'))
        new_depth_mask_path = os.path.join(directory, new_image_name.replace('.png', '_depth_mask.npy'))
        
        np.save(new_depth_path, depth)
        np.save(new_depth_mask_path, depth_mask)

requests.post("https://ntfy.sh/ai_diskintipacchio", data="3/6".encode(encoding='utf-8'))

for filename in os.listdir(directory):
    if filename.endswith('.png') and not ('b' in filename):
        image_path = os.path.join(directory, filename)
        depth_path = os.path.join(directory, filename.replace('.png', '_depth.npy'))
        depth_mask_path = os.path.join(directory, filename.replace('.png', '_depth_mask.npy'))
        
        new_image = modify_brightness(image_path, -25)
        new_image_name = list(filename)
        new_image_name[2] = 'l'
        new_image_name = ''.join(new_image_name)
        new_image_path = os.path.join(directory, new_image_name)
        new_image.save(new_image_path)
        
        depth = np.load(depth_path)
        depth_mask = np.load(depth_mask_path)
        
        new_depth_path = os.path.join(directory, new_image_name.replace('.png', '_depth.npy'))
        new_depth_mask_path = os.path.join(directory, new_image_name.replace('.png', '_depth_mask.npy'))
        
        np.save(new_depth_path, depth)
        np.save(new_depth_mask_path, depth_mask)

requests.post("https://ntfy.sh/ai_diskintipacchio", data="4/6".encode(encoding='utf-8'))

for filename in os.listdir(directory):
    if filename.endswith('.png'):
        image_path = os.path.join(directory, filename)
        depth_path = os.path.join(directory, filename.replace('.png', '_depth.npy'))
        depth_mask_path = os.path.join(directory, filename.replace('.png', '_depth_mask.npy'))
        
        new_image = modify_saturation(image_path, 25)
        new_image_name = list(filename)
        new_image_name[3] = 'k'
        new_image_name = ''.join(new_image_name)
        new_image_path = os.path.join(directory, new_image_name)
        new_image.save(new_image_path)
        
        depth = np.load(depth_path)
        depth_mask = np.load(depth_mask_path)
        
        new_depth_path = os.path.join(directory, new_image_name.replace('.png', '_depth.npy'))
        new_depth_mask_path = os.path.join(directory, new_image_name.replace('.png', '_depth_mask.npy'))
        
        np.save(new_depth_path, depth)
        np.save(new_depth_mask_path, depth_mask)

requests.post("https://ntfy.sh/ai_diskintipacchio", data="5/6".encode(encoding='utf-8'))
    
for filename in os.listdir(directory):
    if filename.endswith('.png') and not ('k' in filename):
        image_path = os.path.join(directory, filename)
        depth_path = os.path.join(directory, filename.replace('.png', '_depth.npy'))
        depth_mask_path = os.path.join(directory, filename.replace('.png', '_depth_mask.npy'))
        
        new_image = modify_saturation(image_path, -25)
        new_image_name = list(filename)
        new_image_name[3] = 'q'
        new_image_name = ''.join(new_image_name)
        new_image_path = os.path.join(directory, new_image_name)
        new_image.save(new_image_path)
        
        depth = np.load(depth_path)
        depth_mask = np.load(depth_mask_path)
        
        new_depth_path = os.path.join(directory, new_image_name.replace('.png', '_depth.npy'))
        new_depth_mask_path = os.path.join(directory, new_image_name.replace('.png', '_depth_mask.npy'))
        
        np.save(new_depth_path, depth)
        np.save(new_depth_mask_path, depth_mask)

requests.post("https://ntfy.sh/ai_diskintipacchio", data="6/6".encode(encoding='utf-8'))