import os
import numpy as np
from shutil import move
from dotenv import load_dotenv
from os.path import join
from os.path import join, dirname

# Load the dataset path from a .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
dir_path = os.environ.get("Dataset")
validation_path = os.environ.get("Validation")

# Calculate the number of triples to move
N = int(len(os.listdir(dir_path)) / 3 * 0.1)

# Get the list of files in the dataset directory
file_list = np.array(os.listdir(dir_path))
np.random.shuffle(file_list)

fixed_length=27
element_names = []

# Iterate over the first N files in the dataset directory
for file in file_list[:N]:
    # Get the base name of the file (up to the 27th character)
    element_name = file[:fixed_length]
    # Check if the base name is not already in the list of element names
    if element_name not in element_names:
        # Add the base name to the list of element names
        element_names.append(element_name)

# Move the triples of files
for element_name in element_names:
    # Move and set permissions for image file
    image_file = join(dir_path, element_name + ".png")
    move(image_file, join(validation_path, element_name + ".png"))
    os.chmod(join(validation_path, element_name + ".png"), 0o777)
    
    # Move and set permissions for depth.npy file
    depth_file = join(dir_path, element_name + "_depth.npy")
    move(depth_file, join(validation_path, element_name + "_depth.npy"))
    os.chmod(join(validation_path, element_name + "_depth.npy"), 0o777)
    
    # Move and set permissions for depth_mask.npy file
    depth_mask_file = join(dir_path, element_name + "_depth_mask.npy")
    move(depth_mask_file, join(validation_path, element_name + "_depth_mask.npy"))
    os.chmod(join(validation_path, element_name + "_depth_mask.npy"), 0o777)