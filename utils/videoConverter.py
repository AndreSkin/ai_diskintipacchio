import cv2
import os
from os.path import join, dirname
from dotenv import load_dotenv
from gradio_client import Client
import shutil

def delete_folder_contents(folder_path):
    for filename in os.listdir(folder_path):
        file_path = os.path.join(folder_path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print(f'Failed to delete {file_path}. Reason: {e}')

dotenv_path = join(dirname(__file__), '../.env')
load_dotenv(dotenv_path)

input_video = os.getenv('videoPath')
pre_frames = os.getenv('pre_frames')
post_frames = os.getenv('post_frames')
output_path = os.getenv('videoResult')
gradio_tmp = os.getenv('gradio_tmp')


delete_folder_contents(gradio_tmp)
delete_folder_contents(pre_frames)
delete_folder_contents(post_frames)



if not os.path.exists(pre_frames):
    os.makedirs(pre_frames)

if not os.path.exists(post_frames):
    os.makedirs(pre_frames)

vidcap = cv2.VideoCapture(input_video)
success,image = vidcap.read()
count = 0
while success:
    cv2.imwrite(os.path.join(pre_frames, "frame%d.jpg" % count), image) # salva il fotogramma come file JPEG nella cartella specificata
    success,image = vidcap.read()
    count += 1


client = Client("http://127.0.0.1:7860/")
for filename in os.listdir(pre_frames):
    result = client.predict(
                    os.path.join(pre_frames, filename),	# str (filepath or URL to image) in 'image' Image component
                    api_name="/predict"
    )
    print(result)
    os.rename(result, os.path.join(post_frames, filename))

image_format = '.jpg' # sostituisci con il formato dei file immagine (ad esempio, '.png', '.jpeg', ecc.)
fps = 30 # sostituisci con il numero di fotogrammi al secondo desiderato

def sort_frames(frame):
    frame_number = int(frame.replace('frame', '').replace(image_format, ''))
    return frame_number

images = sorted([img for img in os.listdir(post_frames) if img.endswith(image_format)], key=sort_frames)
frame = cv2.imread(os.path.join(post_frames, images[0]))
height, width, layers = frame.shape


frame = cv2.imread(os.path.join(post_frames, images[0]))
height, width, layers = frame.shape

video = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(*'mp4v'), fps, (width,height))

for image in images:
    video.write(cv2.imread(os.path.join(post_frames, image)))

cv2.destroyAllWindows()
video.release()