import os
import requests

folder_path = '/public.hpc/diskintipacchio/dataset/shrink/'

for file_name in os.listdir(folder_path):
    file_path = os.path.join(folder_path, file_name)
    if os.path.isfile(file_path):
        current_permissions = oct(os.stat(file_path).st_mode)[-3:]
        if current_permissions != '777':
            os.chmod(file_path, 0o777)

requests.post("https://ntfy.sh/ai_diskintipacchio", data="Dataset 777 finito".encode(encoding='utf-8'))
