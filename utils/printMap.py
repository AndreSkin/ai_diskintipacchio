import os
import numpy as np
from PIL import Image

elem = np.load('/home/alessio/Desktop/conTutto/2023-04-25_01_42_00_25k_4/2023-04-25_01_42_00_25k_4_depth_map_computed_3.npy')
min_val = elem.min()
max_val = elem.max()

print(min_val)
print(max_val)

#Trasforma la matrice in una matrice di colori RGB
rgb_matrix = np.zeros((elem.shape[0], elem.shape[1], 3), dtype=np.uint8)
for i in range(elem.shape[0]):
   for j in range(elem.shape[1]):
#        Mappa il valore della matrice al corrispondente valore RGB
       ratio = (elem[i,j] - min_val) / (max_val - min_val)
       rgb_matrix[i,j,0] = int(ratio * 255)
       rgb_matrix[i,j,1] = int((1 - ratio) * 255)
       rgb_matrix[i,j,2] = int(ratio * 255)
#
#Mostra la matrice RGB come immagine
img = Image.fromarray(rgb_matrix)
img.show()