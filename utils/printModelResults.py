import numpy as np
from matplotlib import pyplot as plt
from PIL import Image

#img = plt.imread('/Users/alessiodipasquale/Projects/ai_diskintipacchio/dataset/00006_00081_indoors_350_010.png')
a = np.load('./pr_mask2.npy')
a.reshape((-1,192,256,2))
elem = a[0]
elem = elem[:, :, 0]



# Matrice di esempio

# Calcola il valore minimo e massimo della matrice
min_val = elem.min()
max_val = elem.max()

print(min_val)
print(max_val)

# Trasforma la matrice in una matrice di colori RGB
# Trasforma la matrice in una matrice di colori RGB
rgb_matrix = np.zeros((elem.shape[0], elem.shape[1], 3), dtype=np.uint8)
for i in range(elem.shape[0]):
    for j in range(elem.shape[1]):
        # Mappa il valore della matrice al corrispondente valore RGB
        ratio = (elem[i,j] - min_val) / (max_val - min_val)
        rgb_matrix[i,j,0] = int(ratio * 255)
        rgb_matrix[i,j,1] = int((1 - ratio) * 255)
        rgb_matrix[i,j,2] = int(ratio * 255)

# Mostra la matrice RGB come immagine
img = Image.fromarray(rgb_matrix)
img.show()