import numpy as np
import tensorflow as tf
# import tflite
from PIL import Image
from dotenv import load_dotenv
import os
from os.path import join, dirname

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
model_path = os.environ.get("modelPathLite")
test_img = os.environ.get("testImgPath")


# Load the TFLite model and allocate tensors.
interpreter = tf.lite.Interpreter(model_path=model_path)
interpreter.allocate_tensors()

# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()


# Carica l'immagine utilizzando PIL
image = Image.open(test_img)

input_shape = input_details[0]['shape'][1:3]
print(input_shape)

resized_image = image.resize((input_shape[1], input_shape[0]))

# Converti l'immagine in un array numpy
input_data = np.array(resized_image, dtype=np.float32)
input_data /= 255.0  # Normalizza i valori dei pixel nell'intervallo [0, 1]

# Aggiungi una dimensione all'array per rappresentare il batch di input
input_data = np.expand_dims(input_data, axis=0)
# Imposta i dati di input
interpreter.set_tensor(input_details[0]['index'], input_data)

# Esegui l'inferenza
interpreter.invoke()

# Ottieni i risultati
output_data = interpreter.get_tensor(output_details[0]['index'])
print(output_data)